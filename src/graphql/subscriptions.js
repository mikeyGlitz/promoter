// @flow
// this is an auto generated file. This will be overwritten

export const onCreatePage = `subscription OnCreatePage {
  onCreatePage {
    id
    title
    content
  }
}
`;
export const onUpdatePage = `subscription OnUpdatePage {
  onUpdatePage {
    id
    title
    content
  }
}
`;
export const onDeletePage = `subscription OnDeletePage {
  onDeletePage {
    id
    title
    content
  }
}
`;
export const onCreatePost = `subscription OnCreatePost {
  onCreatePost {
    id
    title
    content
    owner
    comments {
      items {
        id
        body
        owner
      }
      nextToken
    }
  }
}
`;
export const onUpdatePost = `subscription OnUpdatePost($owner: String) {
  onUpdatePost(owner: $owner) {
    id
    title
    content
    owner
    comments {
      items {
        id
        body
        owner
      }
      nextToken
    }
  }
}
`;
export const onDeletePost = `subscription OnDeletePost($owner: String) {
  onDeletePost(owner: $owner) {
    id
    title
    content
    owner
    comments {
      items {
        id
        body
        owner
      }
      nextToken
    }
  }
}
`;
export const onCreateComment = `subscription OnCreateComment {
  onCreateComment {
    id
    body
    owner
    post {
      id
      title
      content
      owner
      comments {
        nextToken
      }
    }
  }
}
`;
export const onUpdateComment = `subscription OnUpdateComment($owner: String!) {
  onUpdateComment(owner: $owner) {
    id
    body
    owner
    post {
      id
      title
      content
      owner
      comments {
        nextToken
      }
    }
  }
}
`;
export const onDeleteComment = `subscription OnDeleteComment($owner: String) {
  onDeleteComment(owner: $owner) {
    id
    body
    owner
    post {
      id
      title
      content
      owner
      comments {
        nextToken
      }
    }
  }
}
`;
