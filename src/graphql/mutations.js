// @flow
// this is an auto generated file. This will be overwritten

export const createPage = `mutation CreatePage($input: CreatePageInput!) {
  createPage(input: $input) {
    id
    title
    content
  }
}
`;
export const updatePage = `mutation UpdatePage($input: UpdatePageInput!) {
  updatePage(input: $input) {
    id
    title
    content
  }
}
`;
export const deletePage = `mutation DeletePage($input: DeletePageInput!) {
  deletePage(input: $input) {
    id
    title
    content
  }
}
`;
export const createPost = `mutation CreatePost($input: CreatePostInput!) {
  createPost(input: $input) {
    id
    title
    content
    owner
    comments {
      items {
        id
        body
        owner
      }
      nextToken
    }
  }
}
`;
export const updatePost = `mutation UpdatePost($input: UpdatePostInput!) {
  updatePost(input: $input) {
    id
    title
    content
    owner
    comments {
      items {
        id
        body
        owner
      }
      nextToken
    }
  }
}
`;
export const deletePost = `mutation DeletePost($input: DeletePostInput!) {
  deletePost(input: $input) {
    id
    title
    content
    owner
    comments {
      items {
        id
        body
        owner
      }
      nextToken
    }
  }
}
`;
export const createComment = `mutation CreateComment($input: CreateCommentInput!) {
  createComment(input: $input) {
    id
    body
    owner
    post {
      id
      title
      content
      owner
      comments {
        nextToken
      }
    }
  }
}
`;
export const updateComment = `mutation UpdateComment($input: UpdateCommentInput!) {
  updateComment(input: $input) {
    id
    body
    owner
    post {
      id
      title
      content
      owner
      comments {
        nextToken
      }
    }
  }
}
`;
export const deleteComment = `mutation DeleteComment($input: DeleteCommentInput!) {
  deleteComment(input: $input) {
    id
    body
    owner
    post {
      id
      title
      content
      owner
      comments {
        nextToken
      }
    }
  }
}
`;
