/* @flow */
/* eslint-disable */
//  This file was automatically generated and should not be edited.

export type CreatePageInput = {|
  id?: ?string,
  title: string,
  content: string,
|};

export type UpdatePageInput = {|
  id: string,
  title?: ?string,
  content?: ?string,
|};

export type DeletePageInput = {|
  id?: ?string,
|};

export type CreatePostInput = {|
  id?: ?string,
  title: string,
  content: string,
  owner: string,
|};

export type UpdatePostInput = {|
  id: string,
  title?: ?string,
  content?: ?string,
  owner?: ?string,
|};

export type DeletePostInput = {|
  id?: ?string,
|};

export type CreateCommentInput = {|
  id?: ?string,
  body: string,
  owner: string,
  commentPostId?: ?string,
|};

export type UpdateCommentInput = {|
  id: string,
  body?: ?string,
  owner?: ?string,
  commentPostId?: ?string,
|};

export type DeleteCommentInput = {|
  id?: ?string,
|};

export type ModelPageFilterInput = {|
  id?: ?ModelIDFilterInput,
  title?: ?ModelStringFilterInput,
  content?: ?ModelStringFilterInput,
  and?: ?Array< ?ModelPageFilterInput >,
  or?: ?Array< ?ModelPageFilterInput >,
  not?: ?ModelPageFilterInput,
|};

export type ModelIDFilterInput = {|
  ne?: ?string,
  eq?: ?string,
  le?: ?string,
  lt?: ?string,
  ge?: ?string,
  gt?: ?string,
  contains?: ?string,
  notContains?: ?string,
  between?: ?Array< ?string >,
  beginsWith?: ?string,
|};

export type ModelStringFilterInput = {|
  ne?: ?string,
  eq?: ?string,
  le?: ?string,
  lt?: ?string,
  ge?: ?string,
  gt?: ?string,
  contains?: ?string,
  notContains?: ?string,
  between?: ?Array< ?string >,
  beginsWith?: ?string,
|};

export type ModelPostFilterInput = {|
  id?: ?ModelIDFilterInput,
  title?: ?ModelStringFilterInput,
  content?: ?ModelStringFilterInput,
  owner?: ?ModelStringFilterInput,
  and?: ?Array< ?ModelPostFilterInput >,
  or?: ?Array< ?ModelPostFilterInput >,
  not?: ?ModelPostFilterInput,
|};

export type ModelCommentFilterInput = {|
  id?: ?ModelIDFilterInput,
  body?: ?ModelStringFilterInput,
  owner?: ?ModelStringFilterInput,
  and?: ?Array< ?ModelCommentFilterInput >,
  or?: ?Array< ?ModelCommentFilterInput >,
  not?: ?ModelCommentFilterInput,
|};

export type SearchablePostFilterInput = {|
  id?: ?SearchableIDFilterInput,
  title?: ?SearchableStringFilterInput,
  content?: ?SearchableStringFilterInput,
  owner?: ?SearchableStringFilterInput,
  and?: ?Array< ?SearchablePostFilterInput >,
  or?: ?Array< ?SearchablePostFilterInput >,
  not?: ?SearchablePostFilterInput,
|};

export type SearchableIDFilterInput = {|
  ne?: ?string,
  eq?: ?string,
  match?: ?string,
  matchPhrase?: ?string,
  matchPhrasePrefix?: ?string,
  multiMatch?: ?string,
  exists?: ?boolean,
  wildcard?: ?string,
  regexp?: ?string,
|};

export type SearchableStringFilterInput = {|
  ne?: ?string,
  eq?: ?string,
  match?: ?string,
  matchPhrase?: ?string,
  matchPhrasePrefix?: ?string,
  multiMatch?: ?string,
  exists?: ?boolean,
  wildcard?: ?string,
  regexp?: ?string,
|};

export type SearchablePostSortInput = {|
  field?: ?SearchablePostSortableFields,
  direction?: ?SearchableSortDirection,
|};

export type SearchablePostSortableFields =
  "id" |
  "title" |
  "content" |
  "owner";


export type SearchableSortDirection =
  "asc" |
  "desc";


export type CreatePageMutationVariables = {|
  input: CreatePageInput,
|};

export type CreatePageMutation = {|
  createPage: ? {|
    __typename: "Page",
    id: string,
    title: string,
    content: string,
  |},
|};

export type UpdatePageMutationVariables = {|
  input: UpdatePageInput,
|};

export type UpdatePageMutation = {|
  updatePage: ? {|
    __typename: "Page",
    id: string,
    title: string,
    content: string,
  |},
|};

export type DeletePageMutationVariables = {|
  input: DeletePageInput,
|};

export type DeletePageMutation = {|
  deletePage: ? {|
    __typename: "Page",
    id: string,
    title: string,
    content: string,
  |},
|};

export type CreatePostMutationVariables = {|
  input: CreatePostInput,
|};

export type CreatePostMutation = {|
  createPost: ? {|
    __typename: "Post",
    id: string,
    title: string,
    content: string,
    owner: string,
    comments: ? {|
      __typename: string,
      items: ? Array<? {|
        __typename: string,
        id: string,
        body: string,
        owner: string,
      |} >,
      nextToken: ?string,
    |},
  |},
|};

export type UpdatePostMutationVariables = {|
  input: UpdatePostInput,
|};

export type UpdatePostMutation = {|
  updatePost: ? {|
    __typename: "Post",
    id: string,
    title: string,
    content: string,
    owner: string,
    comments: ? {|
      __typename: string,
      items: ? Array<? {|
        __typename: string,
        id: string,
        body: string,
        owner: string,
      |} >,
      nextToken: ?string,
    |},
  |},
|};

export type DeletePostMutationVariables = {|
  input: DeletePostInput,
|};

export type DeletePostMutation = {|
  deletePost: ? {|
    __typename: "Post",
    id: string,
    title: string,
    content: string,
    owner: string,
    comments: ? {|
      __typename: string,
      items: ? Array<? {|
        __typename: string,
        id: string,
        body: string,
        owner: string,
      |} >,
      nextToken: ?string,
    |},
  |},
|};

export type CreateCommentMutationVariables = {|
  input: CreateCommentInput,
|};

export type CreateCommentMutation = {|
  createComment: ? {|
    __typename: "Comment",
    id: string,
    body: string,
    owner: string,
    post: ? {|
      __typename: string,
      id: string,
      title: string,
      content: string,
      owner: string,
      comments: ? {|
        __typename: string,
        nextToken: ?string,
      |},
    |},
  |},
|};

export type UpdateCommentMutationVariables = {|
  input: UpdateCommentInput,
|};

export type UpdateCommentMutation = {|
  updateComment: ? {|
    __typename: "Comment",
    id: string,
    body: string,
    owner: string,
    post: ? {|
      __typename: string,
      id: string,
      title: string,
      content: string,
      owner: string,
      comments: ? {|
        __typename: string,
        nextToken: ?string,
      |},
    |},
  |},
|};

export type DeleteCommentMutationVariables = {|
  input: DeleteCommentInput,
|};

export type DeleteCommentMutation = {|
  deleteComment: ? {|
    __typename: "Comment",
    id: string,
    body: string,
    owner: string,
    post: ? {|
      __typename: string,
      id: string,
      title: string,
      content: string,
      owner: string,
      comments: ? {|
        __typename: string,
        nextToken: ?string,
      |},
    |},
  |},
|};

export type GetPageQueryVariables = {|
  id: string,
|};

export type GetPageQuery = {|
  getPage: ? {|
    __typename: "Page",
    id: string,
    title: string,
    content: string,
  |},
|};

export type ListPagesQueryVariables = {|
  filter?: ?ModelPageFilterInput,
  limit?: ?number,
  nextToken?: ?string,
|};

export type ListPagesQuery = {|
  listPages: ? {|
    __typename: "ModelPageConnection",
    items: ? Array<? {|
      __typename: string,
      id: string,
      title: string,
      content: string,
    |} >,
    nextToken: ?string,
  |},
|};

export type GetPostQueryVariables = {|
  id: string,
|};

export type GetPostQuery = {|
  getPost: ? {|
    __typename: "Post",
    id: string,
    title: string,
    content: string,
    owner: string,
    comments: ? {|
      __typename: string,
      items: ? Array<? {|
        __typename: string,
        id: string,
        body: string,
        owner: string,
      |} >,
      nextToken: ?string,
    |},
  |},
|};

export type ListPostsQueryVariables = {|
  filter?: ?ModelPostFilterInput,
  limit?: ?number,
  nextToken?: ?string,
|};

export type ListPostsQuery = {|
  listPosts: ? {|
    __typename: "ModelPostConnection",
    items: ? Array<? {|
      __typename: string,
      id: string,
      title: string,
      content: string,
      owner: string,
      comments: ? {|
        __typename: string,
        nextToken: ?string,
      |},
    |} >,
    nextToken: ?string,
  |},
|};

export type GetCommentQueryVariables = {|
  id: string,
|};

export type GetCommentQuery = {|
  getComment: ? {|
    __typename: "Comment",
    id: string,
    body: string,
    owner: string,
    post: ? {|
      __typename: string,
      id: string,
      title: string,
      content: string,
      owner: string,
      comments: ? {|
        __typename: string,
        nextToken: ?string,
      |},
    |},
  |},
|};

export type ListCommentsQueryVariables = {|
  filter?: ?ModelCommentFilterInput,
  limit?: ?number,
  nextToken?: ?string,
|};

export type ListCommentsQuery = {|
  listComments: ? {|
    __typename: "ModelCommentConnection",
    items: ? Array<? {|
      __typename: string,
      id: string,
      body: string,
      owner: string,
      post: ? {|
        __typename: string,
        id: string,
        title: string,
        content: string,
        owner: string,
      |},
    |} >,
    nextToken: ?string,
  |},
|};

export type SearchPostsQueryVariables = {|
  filter?: ?SearchablePostFilterInput,
  sort?: ?SearchablePostSortInput,
  limit?: ?number,
  nextToken?: ?string,
|};

export type SearchPostsQuery = {|
  searchPosts: ? {|
    __typename: "SearchablePostConnection",
    items: ? Array<? {|
      __typename: string,
      id: string,
      title: string,
      content: string,
      owner: string,
      comments: ? {|
        __typename: string,
        nextToken: ?string,
      |},
    |} >,
    nextToken: ?string,
  |},
|};

export type OnCreatePageSubscription = {|
  onCreatePage: ? {|
    __typename: "Page",
    id: string,
    title: string,
    content: string,
  |},
|};

export type OnUpdatePageSubscription = {|
  onUpdatePage: ? {|
    __typename: "Page",
    id: string,
    title: string,
    content: string,
  |},
|};

export type OnDeletePageSubscription = {|
  onDeletePage: ? {|
    __typename: "Page",
    id: string,
    title: string,
    content: string,
  |},
|};

export type OnCreatePostSubscription = {|
  onCreatePost: ? {|
    __typename: "Post",
    id: string,
    title: string,
    content: string,
    owner: string,
    comments: ? {|
      __typename: string,
      items: ? Array<? {|
        __typename: string,
        id: string,
        body: string,
        owner: string,
      |} >,
      nextToken: ?string,
    |},
  |},
|};

export type OnUpdatePostSubscriptionVariables = {|
  owner?: ?string,
|};

export type OnUpdatePostSubscription = {|
  onUpdatePost: ? {|
    __typename: "Post",
    id: string,
    title: string,
    content: string,
    owner: string,
    comments: ? {|
      __typename: string,
      items: ? Array<? {|
        __typename: string,
        id: string,
        body: string,
        owner: string,
      |} >,
      nextToken: ?string,
    |},
  |},
|};

export type OnDeletePostSubscriptionVariables = {|
  owner?: ?string,
|};

export type OnDeletePostSubscription = {|
  onDeletePost: ? {|
    __typename: "Post",
    id: string,
    title: string,
    content: string,
    owner: string,
    comments: ? {|
      __typename: string,
      items: ? Array<? {|
        __typename: string,
        id: string,
        body: string,
        owner: string,
      |} >,
      nextToken: ?string,
    |},
  |},
|};

export type OnCreateCommentSubscription = {|
  onCreateComment: ? {|
    __typename: "Comment",
    id: string,
    body: string,
    owner: string,
    post: ? {|
      __typename: string,
      id: string,
      title: string,
      content: string,
      owner: string,
      comments: ? {|
        __typename: string,
        nextToken: ?string,
      |},
    |},
  |},
|};

export type OnUpdateCommentSubscriptionVariables = {|
  owner: string,
|};

export type OnUpdateCommentSubscription = {|
  onUpdateComment: ? {|
    __typename: "Comment",
    id: string,
    body: string,
    owner: string,
    post: ? {|
      __typename: string,
      id: string,
      title: string,
      content: string,
      owner: string,
      comments: ? {|
        __typename: string,
        nextToken: ?string,
      |},
    |},
  |},
|};

export type OnDeleteCommentSubscriptionVariables = {|
  owner?: ?string,
|};

export type OnDeleteCommentSubscription = {|
  onDeleteComment: ? {|
    __typename: "Comment",
    id: string,
    body: string,
    owner: string,
    post: ? {|
      __typename: string,
      id: string,
      title: string,
      content: string,
      owner: string,
      comments: ? {|
        __typename: string,
        nextToken: ?string,
      |},
    |},
  |},
|};