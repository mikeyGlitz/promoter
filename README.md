# Promoter

Promoter is a cloud-based cloud-hosted Content Management System (CMS) which is designed
to be built and run on top of AWS Amplify (an Amazon content hosting app development platform)
This project assumes that the user has an AWS account set up.

## Deploying

If using Visual Studio Code, this application uses remote containers which
can be installed using the extension bar.
The set-up files for the remote containers are located in the `.devcontainer` folder.

AWS deployment requires that a user is set up using IAM.
The user policy will have to have the associated policy detailed below.

```js
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": [
                "lambda:CreateFunction",
                "cloudformation:ListExports",
                "lambda:TagResource",
                "cognito-identity:CreateIdentityPool",
                "lambda:GetFunctionConfiguration",
                "iam:PutRolePolicy",
                "es:DeleteElasticsearchServiceRole",
                "amplify:ListApps",
                "dynamodb:DeleteTable",
                "es:AddTags",
                "cloudformation:DescribeStackEvents",
                "cognito-identity:UntagResource",
                "cognito-identity:UpdateIdentityPool",
                "cloudformation:UpdateStack",
                "lambda:DeleteFunction",
                "s3:DeleteObject",
                "cloudformation:DescribeChangeSet",
                "cloudformation:ListStackResources",
                "iam:GetRole",
                "lambda:ListFunctions",
                "amplify:ListJobs",
                "lambda:GetEventSourceMapping",
                "amplify:DeleteWebHook",
                "cognito-idp:DeleteUserPoolClient",
                "iam:DeleteRole",
                "appsync:*",
                "dynamodb:CreateTable",
                "s3:PutObject",
                "cognito-idp:CreateUserPoolClient",
                "dynamodb:GetShardIterator",
                "cloudformation:DeleteStack",
                "lambda:PublishVersion",
                "lambda:DeleteEventSourceMapping",
                "iam:GetRolePolicy",
                "es:ListElasticsearchInstanceTypes",
                "cloudformation:ValidateTemplate",
                "dynamodb:UpdateTable",
                "cognito-identity:DeleteIdentityPool",
                "cloudformation:CreateUploadBucket",
                "cognito-idp:DeleteUserPool",
                "amplify:DeleteBranch",
                "amplify:UntagResource",
                "dynamodb:ListTables",
                "amplify:CreateDeployment",
                "amplify:UpdateWebHook",
                "amplify:UpdateDomainAssociation",
                "cognito-identity:ListIdentityPools",
                "cognito-idp:CreateUserPool",
                "s3:ListBucket",
                "amplify:CreateDomainAssociation",
                "cloudformation:CreateStackInstances",
                "iam:PassRole",
                "es:DeleteElasticsearchDomain",
                "lambda:ListTags",
                "iam:DeleteRolePolicy",
                "cognito-identity:SetIdentityPoolRoles",
                "cloudformation:DescribeStackSetOperation",
                "amplify:CreateApp",
                "es:DescribeElasticsearchDomains",
                "s3:DeleteBucket",
                "cognito-identity:DescribeIdentityPool",
                "lambda:UpdateEventSourceMapping",
                "cloudformation:ListImports",
                "lambda:UpdateFunctionConfiguration",
                "amplify:ListDomainAssociations",
                "amplify:UpdateBranch",
                "amplify:DeleteApp",
                "es:DescribeElasticsearchDomainConfig",
                "amplify:ListBranches",
                "es:ListDomainNames",
                "s3:PutBucketWebsite",
                "cloudformation:TagResource",
                "iam:UpdateRole",
                "es:GetUpgradeStatus",
                "cloudformation:DeleteStackInstances",
                "cloudformation:ListStackInstances",
                "iam:CreateRole",
                "s3:CreateBucket",
                "amplify:CreateBranch",
                "cloudformation:DescribeStackResource",
                "es:ListTags",
                "amplify:StartJob",
                "cloudformation:UpdateStackSet",
                "cloudformation:ContinueUpdateRollback",
                "cloudformation:ListStackSetOperationResults",
                "amplify:GetWebHook",
                "cognito-idp:UntagResource",
                "dynamodb:DescribeTable",
                "amplify:DeleteJob",
                "cognito-identity:TagResource",
                "cloudformation:CreateStackSet",
                "cloudformation:ExecuteChangeSet",
                "amplify:GetApp",
                "cognito-idp:TagResource",
                "lambda:InvokeFunction",
                "cloudformation:DescribeStackInstance",
                "cognito-idp:UpdateUserPoolClient",
                "cloudformation:DescribeStackResources",
                "amplify:CreateWebHook",
                "s3:PutBucketCORS",
                "cognito-idp:ListUserPools",
                "cloudformation:DescribeStacks",
                "s3:GetObject",
                "es:RemoveTags",
                "amplify:StartDeployment",
                "cloudformation:GetTemplate",
                "es:GetCompatibleElasticsearchVersions",
                "cloudformation:UntagResource",
                "amplify:StopJob",
                "cloudformation:UpdateStackInstances",
                "lambda:GetLayerVersion",
                "cloudformation:ListStackSetOperations",
                "lambda:PublishLayerVersion",
                "es:CreateElasticsearchDomain",
                "lambda:CreateEventSourceMapping",
                "cloudformation:DeleteChangeSet",
                "amplify:GetBranch",
                "amplify:GetDomainAssociation",
                "es:DescribeElasticsearchDomain",
                "dynamodb:TagResource",
                "cognito-idp:UpdateGroup",
                "lambda:DeleteLayerVersion",
                "amplify:UpdateApp",
                "amplify:DeleteDomainAssociation",
                "amplify:GetJob",
                "cloudformation:ListStacks",
                "dynamodb:UntagResource",
                "lambda:GetFunction",
                "amplify:ListWebHooks",
                "cloudformation:DeleteStackSet",
                "cloudformation:DescribeStackSet",
                "cloudformation:ListStackSets",
                "cloudformation:CreateStack",
                "amplify:TagResource",
                "lambda:GetPolicy",
                "es:ListElasticsearchVersions",
                "cloudformation:ListChangeSets"
            ],
            "Resource": "*"
        }
    ]
}
```

If not running inside of the VS Code remote container environment, install AWS CLI
using npm

```bash
npm i -g @aws-amplify/cli
```

Install credentials from the user account created in AWS using the AWS Amplify CLI

```bash
amplify configure
```

The application can then be deployed

```bash
amplify push    # Release backend services
amplify publish # Publish frontend web application
```

## Additional Tasks

This project exposes tasks which can be used to assist development

```bash
npm build       # Builds react files
npm test        # Runs unit tests
npm run docs    # Runs documentation
npm start       # Starts the web application (in develop mode)
```
